using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class BodyType
{
    public uint IdBodyType { get; set; }

    public string? BodyTypeName { get; set; }
}