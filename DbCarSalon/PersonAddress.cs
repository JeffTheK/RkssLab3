﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class PersonAddress
{
     public uint IdPerson { get; set; }

     public uint IdAddress { get; set; }

     public virtual Person IdPersonNavigation { get; set; } = null!;

     public virtual Address IdAddressNavigation { get; set; } = null!;
}