﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Street
{
    public uint IdStreet { get; set; }

    public uint IdCity { get; set; }

    public string StreetName { get; set; } = null!;

    public virtual ICollection<Address> Addresses { get; set; } = new List<Address>();

    public virtual City IdCityNavigation { get; set; } = null!;
}
