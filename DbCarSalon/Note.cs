﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Note
{
    public uint IdNote { get; set; }

    public string NoteName { get; set; } = null!;

    public virtual ICollection<Operation> Operations { get; set; } = new List<Operation>();
}