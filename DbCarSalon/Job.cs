using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Job
{
    public uint IdJob { get; set; }

    public string JobName { get; set; } = null!;

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();

    public virtual ICollection<CarSalon> CarSalons { get; set; } = new List<CarSalon>();
}