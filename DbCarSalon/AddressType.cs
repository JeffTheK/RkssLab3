﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class AddressType
{
    public uint IdAddressType { get; set; }

    public string AddressTypeName { get; set; } = null!;
}