﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class PersonContact
{
    public uint IdPersonContact { get; set; }

    public uint IdPerson { get; set;}

    public uint IdContactType { get; set; }

    public string ContactValue { get; set; } = null!;

    public virtual Person IdPersonNavigation { get; set; } = null!;

    public virtual ContactType IdContactTypeNavigation { get; set; } = null!;
}