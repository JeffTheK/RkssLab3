using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class TechnicalDetails
{
     public uint IdTechnicalDetails { get; set; }

     public uint IdBodyType { get; set; }

     public uint IdEngineType { get; set; }

     public uint IdEngineTypeLocation { get; set; }

     public uint NumberOfDoors { get; set; }

     public uint NumberOfSeats { get; set; }

     public uint EngineDisplacement { get; set; }

     public virtual BodyType IdBodyTypeNavigation { get; set; } = null!;

     public virtual EngineType IdEngineTypeNavigation { get; set; } = null!;

     public virtual EngineTypeLocation IdEngineTypeLocationNavigation { get; set; } = null!;
}