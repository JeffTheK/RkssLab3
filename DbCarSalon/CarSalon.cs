using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class CarSalon
{
     public uint IdCarSalon { get; set; }

     public string SalonName { get; set; } = null!;

     public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();

     public virtual ICollection<Address> Addresses { get; set; } = new List<Address>();

     public virtual ICollection<Operation> Operations { get; set; } = new List<Operation>();
}