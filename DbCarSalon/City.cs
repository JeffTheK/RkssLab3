﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class City
{
    public uint IdCity { get; set; }

    public uint IdRegion { get; set; }

    public string CityName { get; set; } = null!;

    public virtual Region IdRegionNavigation { get; set; } = null!;

    public virtual ICollection<Street> Streets { get; set; } = new List<Street>();
}
