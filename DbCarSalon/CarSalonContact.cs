﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class CarSalonContact
{
    public uint IdCarSalon { get; set; }

    public uint IdContactType { get; set; }

    public string ContactValue { get; set; } = null!;

    public virtual CarSalon IdCarSalonNavigation { get; set; } = null!;

    public virtual ContactType IdContactTypeNavigation { get; set; } = null!;
}