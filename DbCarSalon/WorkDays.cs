﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class WorkDays
{
    public uint IdWorkDays { get; set; }

    public string DayName { get; set; } = null!;

    public virtual ICollection<WorkSchedule> WorkSchedules { get; set; } = new List<WorkSchedule>();
}