using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class EngineTypeLocation
{
    public uint IdEngineTypeLocation { get; set; }

    public string EngineLocationTypeName { get; set; } = null!;
}