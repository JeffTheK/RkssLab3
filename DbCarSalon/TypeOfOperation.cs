﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class TypeOfOperation
{
    public uint IdTypeOfOperation { get; set; }

    public string TypeName { get; set; } = null!;

    public virtual ICollection<Operation> Operations { get; set; } = new List<Operation>();
}