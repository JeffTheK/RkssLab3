using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class EngineType
{
    public uint IdEngineType { get; set; }

    public string EngineTypeName { get; set; } = null!;
}