﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Operation
{
	public uint IdOperation { get; set; }

	public uint IdCar { get; set; }

	public uint IdPerson { get; set; }

	public uint IdTypeOfOperation { get; set; }

	public uint IdEmployee {get; set;}

	public uint IdCarSalon { get; set; }

	public uint IdNote { get; set; }

	public DateTime OperationDate { get; set; }
	
	public virtual Car IdCarNavigation { get; set; } = null!;

	public virtual Person IdPersonNavigation { get; set; } = null!;

	public virtual TypeOfOperation IdTypeOfOperationNavigation { get; set; } = null!;

	public virtual Employee IdEmployeeNavigation { get; set; } = null!;

	public virtual CarSalon IdCarSalonNavigation { get; set; } = null!;

	public virtual Note IdNoteNavigation { get; set; } = null!;

	
}