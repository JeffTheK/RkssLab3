﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Region
{
    public uint IdRegion { get; set; }

    public uint IdCountry { get; set; }

    public string RegionName { get; set; } = null!;

    public virtual ICollection<City> Cities { get; set; } = new List<City>();

    public virtual Country IdCountryNavigation { get; set; } = null!;
}
