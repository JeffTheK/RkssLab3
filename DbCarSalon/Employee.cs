﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Employee
{
    public uint IdEmployee { get; set; }

    public uint IdPerson { get; set; }

    public uint IdCarSalon { get; set; }

    public uint IdJob { get; set; }

    public virtual CarSalon IdCarSalonNavigation { get; set; } = null!;

    public virtual Person IdPersonNavigation { get; set; } = null!;

    public virtual Job IdJobNavigation { get; set; } = null!;

}
