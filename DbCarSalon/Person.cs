﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Person
{
    public uint IdPerson { get; set; }

    public string FirstName { get; set; } = null!;

    public string MiddleName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public DateTime BirthDate { get; set; }

    public sbyte Sex { get; set; }

    public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();

    public virtual ICollection<PersonAddress> PersonAddresses { get; set; } = new List<PersonAddress>();
}
