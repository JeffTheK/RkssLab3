using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Sex
{
    public uint SexId { get; set; }

    public string Description { get;set; } = null!;

    public virtual ICollection<Person> Persons { get; set; } = new List<Person>();
}