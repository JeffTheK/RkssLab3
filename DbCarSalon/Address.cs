﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Address
{
    public uint IdAddress { get; set; }

    public uint IdStreet { get; set; }

    public string BuildingNumber { get; set; } = null!;

    public string? FlatNumber { get; set; }

    public virtual ICollection<CarSalon> CarSalons { get; set; } = new List<CarSalon>();

    public virtual Street IdStreetNavigation { get; set; } = null!;

    public virtual ICollection<PersonAddress> PersonAddresses { get; set; } = new List<PersonAddress>();
}
