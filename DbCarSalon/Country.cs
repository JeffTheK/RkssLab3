using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Country
{
    public uint IdCountry { get; set; }

    public string CountryName { get; set; } = null!;
}