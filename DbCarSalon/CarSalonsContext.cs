using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace RkssLab3.DbCarSalon;

public partial class CarSalonsContext : DbContext
{
    public CarSalonsContext()
    {
    }

    public CarSalonsContext(DbContextOptions<CarSalonsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Address> Addresses { get; set; }
    public virtual DbSet<AddressType> AddressTypes  { get; set; }

    public virtual DbSet<BodyType> BodyTypes  { get; set; }

    public virtual DbSet<Car> Cars  { get; set; }

    public virtual DbSet<CarBrand> CarBrands   { get; set; }

    public virtual DbSet<CarModel> CarModels  { get; set; }

    public virtual DbSet<CarSalon> CarSalons  { get; set; }

    public virtual DbSet<CarSalonAddress> CarSalonAddresses  { get; set; }

    public virtual DbSet<CarSalonContact> CarSalonContacts  { get; set; }

    public virtual DbSet<City> Cities  { get; set; }

    public virtual DbSet<Color> Colors  { get; set; }

    public virtual DbSet<ContactType> ContactTypes  { get; set; }

    public virtual DbSet<Country> Countries  { get; set; }

    public virtual DbSet<Employee> Employees  { get; set; }

    public virtual DbSet<EngineType> EngineTypes  { get; set; }

    public virtual DbSet<EngineTypeLocation> EngineTypeLocations  { get; set; }

    public virtual DbSet<Job> Jobs  { get; set; }

    public virtual DbSet<Note> Notes  { get; set; }

    public virtual DbSet<Operation> Operations  { get; set; }

    public virtual DbSet<Person> People  { get; set; }

    public virtual DbSet<PersonAddress> PersonAddresses  { get; set; }

    public virtual DbSet<PersonContact> PersonContacts  { get; set; }

    public virtual DbSet<Region> Regions  { get; set; }

    public virtual DbSet<Sex> Sexes  { get; set; }

    public virtual DbSet<Street> Streets  { get; set; }

    public virtual DbSet<TechnicalDetails> TechnicalDetails  { get; set; }

    public virtual DbSet<TypeOfOperation> TypeOfOperations  { get; set; }

    public virtual DbSet<WorkDays> WorkDays  { get; set; }

    public virtual DbSet<WorkSchedule> WorkSchedules  { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("server=localhost;database=car_salon_cs;user=root;password=123;CharSet=utf8;");
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Address>(entity =>
        {
            entity.HasKey(e => e.IdAddress).HasName("PRIMARY");

            entity.ToTable("address");

            entity.HasIndex(e => e.IdStreet, "id_street");

            entity.Property(e => e.IdAddress).HasColumnName("id_address");
            entity.Property(e => e.BuildingNumber)
                .HasMaxLength(10)
                .HasColumnName("building_number");
            entity.Property(e => e.FlatNumber)
                .HasMaxLength(10)
                .HasColumnName("flat_number");
            entity.Property(e => e.IdStreet).HasColumnName("id_street");

            entity.HasOne(d => d.IdStreetNavigation).WithMany(p => p.Addresses)
                .HasForeignKey(d => d.IdStreet)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("address_ibfk_1");
        });

        modelBuilder.Entity<City>(entity =>
        {
            entity.HasKey(e => e.IdCity).HasName("PRIMARY");

            entity.ToTable("city");

            entity.HasIndex(e => e.CityName, "city_name").IsUnique();

            entity.HasIndex(e => e.IdRegion, "id_region");

            entity.Property(e => e.IdCity).HasColumnName("id_city");
            entity.Property(e => e.CityName)
                .HasMaxLength(60)
                .HasColumnName("city_name");
            entity.Property(e => e.IdRegion).HasColumnName("id_region");

            entity.HasOne(d => d.IdRegionNavigation).WithMany(p => p.Cities)
                .HasForeignKey(d => d.IdRegion)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("city_ibfk_1");
        });

        modelBuilder.Entity<Person>(entity =>
        {
            entity.HasKey(e => e.PersonAddresses).HasName("PRIMARY");

            entity.ToTable("component_directory");

            entity.Property(e => e.PersonAddresses).HasColumnName("id_component_directory");
            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .HasColumnName("name_component");
        });

        modelBuilder.Entity<ContactType>(entity =>
        {
            entity.HasKey(e => e.IdContactType).HasName("PRIMARY");

            entity.ToTable("car_salon");

            entity.HasIndex(e => e.IdContactType, "id_address");

            entity.Property(e => e.IdContactType).HasColumnName("id_car_salon");
        });

        modelBuilder.Entity<CarSalon>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("car_salon_contact");

            entity.HasIndex(e => e.IdCarSalon, "id_car_salon");

            entity.HasIndex(e => e.IdCarSalon, "id_contact_type");

            entity.Property(e => e.IdCarSalon)
                .HasMaxLength(100)
                .HasColumnName("contact_value");
            entity.Property(e => e.IdCarSalon).HasColumnName("id_car_salon");
            entity.Property(e => e.IdCarSalon).HasColumnName("id_contact_type");
        });

        modelBuilder.Entity<ContactType>(entity =>
        {
            entity.HasKey(e => e.IdContactType).HasName("PRIMARY");

            entity.ToTable("contact_type");

            entity.HasIndex(e => e.ContactTypeName, "contact_type_name").IsUnique();

            entity.Property(e => e.IdContactType).HasColumnName("id_contact_type");
            entity.Property(e => e.ContactTypeName)
                .HasMaxLength(60)
                .HasColumnName("contact_type_name");
        });

        modelBuilder.Entity<Country>(entity =>
        {
            entity.HasKey(e => e.IdCountry).HasName("PRIMARY");

            entity.ToTable("country");

            entity.HasIndex(e => e.CountryName, "country_name").IsUnique();

            entity.Property(e => e.IdCountry).HasColumnName("id_country");
            entity.Property(e => e.CountryName)
                .HasMaxLength(50)
                .HasColumnName("country_name");
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.IdEmployee).HasName("PRIMARY");

            entity.ToTable("employee");

            entity.HasIndex(e => e.IdCarSalon, "id_car_salon");

            entity.HasIndex(e => e.IdPerson, "id_person");

            entity.HasIndex(e => e.IdJob, "id_position");

            entity.Property(e => e.IdEmployee).HasColumnName("id_employee");
            entity.Property(e => e.IdCarSalon).HasColumnName("id_car_salon");
            entity.Property(e => e.IdPerson).HasColumnName("id_person");
            entity.Property(e => e.IdJob).HasColumnName("id_position");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany(p => p.Employees)
                .HasForeignKey(d => d.IdCarSalon)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("employee_ibfk_2");

            entity.HasOne(d => d.IdPersonNavigation).WithMany(p => p.Employees)
                .HasForeignKey(d => d.IdPerson)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("employee_ibfk_1");

            entity.HasOne(d => d.IdJobNavigation).WithMany(p => p.Employees)
                .HasForeignKey(d => d.IdJob)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("employee_ibfk_3");
        });

        modelBuilder.Entity<CarSalon>(entity =>
        {
            entity.HasKey(e => e.IdCarSalon).HasName("PRIMARY");

            entity.ToTable("name_product");

            entity.Property(e => e.IdCarSalon).HasColumnName("id_name_product");
            entity.Property(e => e.IdCarSalon)
                .HasMaxLength(50)
                .HasColumnName("name_product");
        });

        modelBuilder.Entity<Note>(entity =>
        {
            entity.HasKey(e => e.IdNote).HasName("PRIMARY");

            entity.ToTable("note");

            entity.Property(e => e.IdNote).HasColumnName("id_note");
            entity.Property(e => e.NoteName)
                .HasMaxLength(255)
                .HasColumnName("name_note");
        });

        modelBuilder.Entity<Operation>(entity =>
        {
            entity.HasKey(e => e.IdOperation).HasName("PRIMARY");

            entity.ToTable("operation");

            entity.HasIndex(e => e.IdCarSalon, "id_car_salon");

            entity.HasIndex(e => e.IdEmployee, "id_employee");

            entity.HasIndex(e => e.IdNote, "id_note");

            entity.HasIndex(e => e.IdPerson, "id_person");

            entity.HasIndex(e => e.IdCar, "id_product");

            entity.HasIndex(e => e.IdTypeOfOperation, "id_type_of_operation");

            entity.Property(e => e.IdOperation).HasColumnName("id_operation");
            entity.Property(e => e.OperationDate)
                .HasColumnType("date")
                .HasColumnName("date_operation");
            entity.Property(e => e.IdCarSalon).HasColumnName("id_car_salon");
            entity.Property(e => e.IdEmployee).HasColumnName("id_employee");
            entity.Property(e => e.IdNote).HasColumnName("id_note");
            entity.Property(e => e.IdPerson).HasColumnName("id_person");
            entity.Property(e => e.IdCar).HasColumnName("id_product");
            entity.Property(e => e.IdTypeOfOperation).HasColumnName("id_type_of_operation");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany(p => p.Operations)
                .HasForeignKey(d => d.IdCarSalon)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("operation_ibfk_5");


            entity.HasOne(d => d.IdNoteNavigation).WithMany(p => p.Operations)
                .HasForeignKey(d => d.IdNote)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("operation_ibfk_6");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany(p => p.Operations)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("operation_ibfk_1");

            entity.HasOne(d => d.IdTypeOfOperationNavigation).WithMany(p => p.Operations)
                .HasForeignKey(d => d.IdTypeOfOperation)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("operation_ibfk_3");
        });

        modelBuilder.Entity<Person>(entity =>
        {
            entity.HasKey(e => e.IdPerson).HasName("PRIMARY");

            entity.ToTable("person");

            entity.Property(e => e.IdPerson).HasColumnName("id_person");
            entity.Property(e => e.BirthDate)
                .HasColumnType("date")
                .HasColumnName("birth_date");
            entity.Property(e => e.FirstName)
                .HasMaxLength(50)
                .HasColumnName("first_name");
            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .HasColumnName("last_name");
            entity.Property(e => e.MiddleName)
                .HasMaxLength(50)
                .HasColumnName("middle_name");
            entity.Property(e => e.Sex).HasColumnName("sex");
        });

        modelBuilder.Entity<PersonAddress>(entity =>
        {
            entity.HasKey(e => e.IdPerson).HasName("PRIMARY");

            entity.ToTable("person_address");

            entity.HasIndex(e => e.IdAddress, "id_address");

            entity.HasIndex(e => e.IdPerson, "id_person");

            entity.Property(e => e.IdPerson).HasColumnName("id_person_address");
            entity.Property(e => e.IdAddress).HasColumnName("id_address");
            entity.Property(e => e.IdPerson).HasColumnName("id_person");

            entity.HasOne(d => d.IdAddressNavigation).WithMany(p => p.PersonAddresses)
                .HasForeignKey(d => d.IdAddress)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("person_address_ibfk_2");

            entity.HasOne(d => d.IdPersonNavigation).WithMany(p => p.PersonAddresses)
                .HasForeignKey(d => d.IdPerson)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("person_address_ibfk_1");
        });

        modelBuilder.Entity<PersonContact>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("person_contact");

            entity.HasIndex(e => e.IdContactType, "id_contact_type");

            entity.HasIndex(e => e.IdPerson, "id_person");

            entity.Property(e => e.ContactValue)
                .HasMaxLength(100)
                .HasColumnName("contact_value");
            entity.Property(e => e.IdContactType).HasColumnName("id_contact_type");
            entity.Property(e => e.IdPerson).HasColumnName("id_person");

            entity.HasOne(d => d.IdContactTypeNavigation).WithMany()
                .HasForeignKey(d => d.IdContactType)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("person_contact_ibfk_2");

            entity.HasOne(d => d.IdPersonNavigation).WithMany()
                .HasForeignKey(d => d.IdPerson)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("person_contact_ibfk_1");
        });

        modelBuilder.Entity<Job>(entity =>
        {
            entity.HasKey(e => e.IdJob).HasName("PRIMARY");

            entity.ToTable("position");

            entity.HasIndex(e => e.JobName, "name_position").IsUnique();

            entity.Property(e => e.IdJob).HasColumnName("id_position");
            entity.Property(e => e.JobName)
                .HasMaxLength(50)
                .HasColumnName("name_position");
        });

        modelBuilder.Entity<CarBrand>(entity =>
        {
            entity.HasKey(e => e.IdCarBrand).HasName("PRIMARY");

            entity.ToTable("producer");

            entity.Property(e => e.IdCarBrand).HasColumnName("id_producer");
            entity.Property(e => e.IdCarBrand)
                .HasMaxLength(50)
                .HasColumnName("name_producer");
        });

        modelBuilder.Entity<CarSalonContact>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("producer_contact");

            entity.HasIndex(e => e.IdContactType, "id_contact_type");

            entity.HasIndex(e => e.IdCarSalon, "id_producer");

            entity.Property(e => e.ContactValue)
                .HasMaxLength(100)
                .HasColumnName("contact_value");
            entity.Property(e => e.IdContactType).HasColumnName("id_contact_type");
            entity.Property(e => e.IdCarSalon).HasColumnName("id_producer");

            entity.HasOne(d => d.IdContactTypeNavigation).WithMany()
                .HasForeignKey(d => d.IdContactType)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("producer_contact_ibfk_2");
        });

        modelBuilder.Entity<Car>(entity =>
        {
            entity.HasKey(e => e.IdCar).HasName("PRIMARY");

            entity.ToTable("product");

            entity.HasIndex(e => e.IdCarModel, "code_product").IsUnique();

            entity.HasIndex(e => e.IdCarBrand, "id_name_product");

            entity.HasIndex(e => e.IdColor, "id_producer");

            entity.Property(e => e.IdCountry).HasColumnName("id_product");
            entity.Property(e => e.IdTechnicalDetails)
                .HasMaxLength(250)
                .HasColumnName("additional_descr");
            entity.Property(e => e.IdCarModelNavigation).HasColumnName("code_product");
            entity.Property(e => e.IdCarModelNavigation).HasColumnName("guarantee_months");
            entity.Property(e => e.Availability).HasColumnName("id_name_product");
            entity.Property(e => e.Availability).HasColumnName("id_producer");
            entity.Property(e => e.PublishedYears)
                .HasColumnType("double(10,2)")
                .HasColumnName("price_usd");
        });

        modelBuilder.Entity<Operation>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("product_character");

            entity.HasIndex(e => e.IdEmployeeNavigation, "id_product");

            entity.HasIndex(e => e.IdCar, "id_technical_details");

            entity.Property(e => e.IdNote).HasColumnName("id_product");
            entity.Property(e => e.IdPersonNavigation).HasColumnName("id_technical_details");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany()
                .HasForeignKey(d => d.IdCarSalon)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("product_character_ibfk_2");
        });

        modelBuilder.Entity<Region>(entity =>
        {
            entity.HasKey(e => e.IdRegion).HasName("PRIMARY");

            entity.ToTable("region");

            entity.HasIndex(e => e.IdCountry, "id_country");

            entity.HasIndex(e => e.RegionName, "region_name").IsUnique();

            entity.Property(e => e.IdRegion).HasColumnName("id_region");
            entity.Property(e => e.IdCountry).HasColumnName("id_country");
            entity.Property(e => e.RegionName)
                .HasMaxLength(60)
                .HasColumnName("region_name");
        });

        modelBuilder.Entity<Street>(entity =>
        {
            entity.HasKey(e => e.IdStreet).HasName("PRIMARY");

            entity.ToTable("street");

            entity.HasIndex(e => e.IdCity, "id_city");

            entity.HasIndex(e => e.StreetName, "street_name").IsUnique();

            entity.Property(e => e.IdStreet).HasColumnName("id_street");
            entity.Property(e => e.IdCity).HasColumnName("id_city");
            entity.Property(e => e.StreetName)
                .HasMaxLength(80)
                .HasColumnName("street_name");

            entity.HasOne(d => d.IdCityNavigation).WithMany(p => p.Streets)
                .HasForeignKey(d => d.IdCity)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("street_ibfk_1");
        });

        modelBuilder.Entity<TechnicalDetails>(entity =>
        {
            entity.HasKey(e => e.IdTechnicalDetails).HasName("PRIMARY");

            entity.ToTable("technical_details");

            entity.HasIndex(e => e.IdBodyType, "id_component_directory");

            entity.HasIndex(e => e.IdBodyType, "type_component").IsUnique();

            entity.Property(e => e.IdTechnicalDetails).HasColumnName("id_technical_details");
            entity.Property(e => e.IdEngineTypeLocation).HasColumnName("id_component_directory");
            entity.Property(e => e.IdEngineTypeLocation)
                .HasMaxLength(100)
                .HasColumnName("type_component");
        });

        modelBuilder.Entity<TypeOfOperation>(entity =>
        {
            entity.HasKey(e => e.IdTypeOfOperation).HasName("PRIMARY");

            entity.ToTable("type_of_operation");

            entity.HasIndex(e => e.TypeName, "name_type").IsUnique();

            entity.Property(e => e.IdTypeOfOperation).HasColumnName("id_type_of_operation");
            entity.Property(e => e.TypeName)
                .HasMaxLength(50)
                .HasColumnName("name_type");
        });

        modelBuilder.Entity<CarBrand>(entity =>
        {
            entity.HasKey(e => e.IdCarBrand).HasName("PRIMARY");

            entity.ToTable("producer");

            entity.Property(e => e.IdCarBrand).HasColumnName("id_producer");
            entity.Property(e => e.IdCarBrand)
                .HasMaxLength(50)
                .HasColumnName("name_producer");
        });

        modelBuilder.Entity<WorkDays>(entity =>
        {
            entity.HasKey(e => e.DayName).HasName("PRIMARY");

            entity.ToTable("work_day");

            entity.Property(e => e.IdWorkDays).HasColumnName("id_work_day");
            entity.Property(e => e.DayName)
                .HasMaxLength(50)
                .HasColumnName("name_day");
        });

        modelBuilder.Entity<WorkSchedule>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("work_schedule");

            entity.HasIndex(e => e.IdCarSalon, "id_car_salon");

            entity.HasIndex(e => e.IdWorkSchedule, "id_work_day");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany()
                .HasForeignKey(d => d.IdCarSalon)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("work_schedule_ibfk_1");

            entity.HasOne(d => d.IdCarSalonNavigation).WithMany()
                .HasForeignKey(d => d.IdWorkSchedule)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("work_schedule_ibfk_2");
        });

        modelBuilder.Entity<ContactType>(entity =>
        {
            entity.HasKey(e => e.IdContactType).HasName("PRIMARY");

            entity.ToTable("contact_type");

            entity.HasIndex(e => e.ContactTypeName, "contact_type_name").IsUnique();

            entity.Property(e => e.IdContactType).HasColumnName("id_contact_type");
            entity.Property(e => e.ContactTypeName)
                .HasMaxLength(60)
                .HasColumnName("contact_type_name");
        });

        modelBuilder.Entity<Country>(entity =>
        {
            entity.HasKey(e => e.IdCountry).HasName("PRIMARY");

            entity.ToTable("country");

            entity.HasIndex(e => e.CountryName, "country_name").IsUnique();

            entity.Property(e => e.IdCountry).HasColumnName("id_country");
            entity.Property(e => e.CountryName)
                .HasMaxLength(50)
                .HasColumnName("country_name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}