using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class CarBrand
{
    public uint IdCarBrand { get; set; }

    public string? CarBrandName { get; set; } = null!;

    public virtual ICollection<Car> Cars { get; set; } = new List<Car>();
}