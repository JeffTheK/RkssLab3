﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class ContactType
{
    public uint IdContactType { get; set; }

    public string ContactTypeName { get; set; } = null!;
}
