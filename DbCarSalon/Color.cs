using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Color
{
    public uint IdColor { get; set; }

    public string ColorName { get; set; } = null!;

    public virtual ICollection<Color> Colors { get; set; } = new List<Color>();
}