using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class CarModel
{
    public uint IdCarModel { get; set; }

    public string? CarModelName { get; set; } = null!;
}