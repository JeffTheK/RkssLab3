﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class Car
{
    public uint IdCar { get; set; }

    public uint IdCarBrand { get; set; }

    public uint IdCountry { get; set; }

    public uint IdCarModel { get; set; }

    public uint IdColor { get; set; }

    public uint IdTechnicalDetails { get; set; }

    public sbyte Availability { get; set; }

    public DateTime PublishedYears { get; set; }

    public virtual CarBrand IdCarBrandNavigation { get; set; } = null!;

    public virtual Country IdCountryNavigation { get; set; } = null!;

    public virtual CarModel IdCarModelNavigation { get; set; } = null!;

    public virtual Color IdColorNavigation { get; set; } = null!;

    public virtual TechnicalDetails IdTechnicalDetailsNavigation { get; set; } = null!;

    public virtual ICollection<Operation> Operations { get; set; } = new List<Operation>();
}
