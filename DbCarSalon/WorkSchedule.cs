﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class WorkSchedule
{
     public uint IdWorkSchedule { get; set; }

     public uint IdCarSalon { get; set; }

     public virtual CarSalon IdCarSalonNavigation { get; set; } = null!;
}