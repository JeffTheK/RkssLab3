﻿using System;
using System.Collections.Generic;

namespace RkssLab3.DbCarSalon;

public partial class CarSalonAddress
{
     public uint IdCarSalon { get; set; }

     public uint IdAddress { get; set; }

     public virtual CarSalon IdCarSalonNavigation { get; set; } = null!;

     public virtual Address IdAddressNavigation { get; set; } = null!;
}